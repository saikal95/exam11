import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActionReducer, MetaReducer, StoreModule} from '@ngrx/store';
import {ProductsComponent} from './products/products.component';
import {EffectsModule} from '@ngrx/effects';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {FlexModule} from "@angular/flex-layout";
import {LayoutComponent} from './ui/layout/layout.component';
import {FileInputComponent} from "./ui/file-input/file-input.component";
import {FormsModule} from "@angular/forms";
import {CenteredCardComponent} from "./ui/centered-card/centered-card.component";
import {MatCardModule} from "@angular/material/card";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {ImagePipe} from "./pipes/image.pipe";
import {usersReducer} from "./store/user.reducer";
import {productReducer} from "./store/products.reducer";
import {categoryReducer} from "./store/category.reducer";
import {UsersEffects} from "./store/user.effects";
import {ProductsEffects} from "./store/products.effects";
import {CategoryEffects} from "./store/category.effects";
import {localStorageSync} from "ngrx-store-localstorage";
import {HttpClientModule} from "@angular/common/http";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {RegisterComponent} from './register/register.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {LoginComponent} from './login/login.component';
import {AddNewComponent} from './add-new/add-new.component';
import { YComponent } from './y/y.component';


const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers : MetaReducer[] = [localStorageSyncReducer];
@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LayoutComponent,
    FileInputComponent,
    CenteredCardComponent,
    ImagePipe,
    RegisterComponent,
    LoginComponent,
    AddNewComponent,
    YComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({users: usersReducer, products: productReducer, categories: categoryReducer}, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, ProductsEffects, CategoryEffects]),
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSnackBarModule,
    FlexModule,
    FormsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
