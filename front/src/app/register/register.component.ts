import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {RegisterError} from "../models/user.model";
import {Observable, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {registerUserRequest} from "../store/user.actions";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  @ViewChild('f') form!: NgForm;
  error: Observable<RegisterError | null>;
  errorSub!: Subscription;
  loading: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.users.registerError);
    this.loading = store.select(state=> state.users.registerLoading);
  }

    ngOnInit(): void {
    // this.errorSub = this.error.subscribe(error => {
    //   if(error){
    //     const msg = error.errors.email.message;
    //     this.form.form.get('email')?.setErrors({serverError: msg});
    //   }else {
    //     this.form.form.get('email')?.setErrors({});
    //   }
    // })
  }

  onSubmit() {
    this.store.dispatch(registerUserRequest({userData: this.form.value}));
    console.log(this.form.value);
  }

}
