import { Component, OnInit } from '@angular/core';
import {map, Observable, shareReplay} from "rxjs";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {User} from "../models/user.model";
import {Category} from "../models/category.model";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {logoutUserRequest} from "../store/user.actions";
import {fetchCategoryRequest} from "../store/category.actions";

@Component({
  selector: 'app-y',
  templateUrl: './y.component.html',
  styleUrls: ['./y.component.sass']
})
export class YComponent{
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  categories: Observable<Category[]>

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {
    this.categories = store.select(state => state.categories.categories);
  }

  logout() {
    this.store.dispatch(fetchCategoryRequest());
  }

}
