import { Component, OnInit } from '@angular/core';
import {map, Observable, shareReplay} from "rxjs";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {User} from "../../models/user.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {logoutUserRequest} from "../../store/user.actions";
import {Product} from "../../models/product.model";
import {Category} from "../../models/category.model";
import {fetchCategoryRequest} from "../../store/category.actions";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent  implements OnInit{
  user: Observable<null | User>;
  categories: Observable<Category[]>

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
    this.categories = store.select(state => state.categories.categories);
  }

  logout() {
    this.store.dispatch(logoutUserRequest());
  }

  ngOnInit() {
    this.store.dispatch(fetchCategoryRequest());
  }

}
