import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs";
import {ApiCategoryData, Category} from "../models/category.model";
import {ApiProductData, Product} from "../models/product.model";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) {
  }


  getCategories() {
    return this.http.get<ApiCategoryData[]>(environment.apiUrl + '/products').pipe(
      map(response => {
        return response.map(categoryData => {
          return new Category(
            categoryData._id,
            categoryData.typeOf,
          );
        });
      })
    );
  }








}
