import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ApiProductData, Product, ProductData} from "../models/product.model";
import {environment} from "../../environments/environment";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get<ApiProductData[]>(environment.apiUrl + '/products').pipe(
      map(response => {
        return response.map(productData => {
          return new Product(
            productData._id,
            productData.title,
            productData.description,
            productData.price,
            productData.image,
            productData.category,
          );
        });
      })
    );
  }

  createProduct(productData: ProductData, token : string) {
    const formData = new FormData();
    Object.keys(productData).forEach(key => {
      if (productData[key] !== null) {
        formData.append(key, productData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/products', formData, {
      headers: new HttpHeaders({'Authorization' : token}),
    });
  }

  getProduct(id: string) {
    return this.http.get<Product>(environment.apiUrl + '/products/' + id).pipe(
      map(response => {
        return response;
      })
    )

  }

  deleteProduct(id: string, token: string){
    return this.http.delete<Product>(environment.apiUrl + '/products/' + id,{
      headers: new HttpHeaders({'Authorization' : token}),
    })

  }



}



