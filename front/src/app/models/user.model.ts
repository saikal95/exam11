export interface User{
  _id: string,
  userName: string,
  password: string,
  displayName: string,
  phoneNumber: string,
  token: string

}


export interface RegisterUserData {
  userName: string,
  password: string,
  displayName: string,
  phoneNumber: string,

}


export interface LoginUserData{
  displayName: string,
  password: string,
}


export interface FieldError{
  message: string
}


export interface RegisterError{
  errors: {
    displayName: FieldError,
    password: FieldError,
  }

}

export interface LoginError{
  error: string,
}


