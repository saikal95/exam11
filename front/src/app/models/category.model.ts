export class Category {
  constructor(
    public _id: string,
    public typeOf: string,
  ) {}
}




export interface Category{
  _id: string,
  typeOf: string,
}


export interface ApiCategoryData {
  _id: string,
  typeOf: string,

}
