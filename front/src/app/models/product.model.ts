

export class Product {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public price: number,
    public image: string,
    public category: string,
  ) {}
}

export interface ProductData {
  [key: string]: any;
  title: string;
  description: string;
  image: File | null;
  category: string,
}

export interface ApiProductData {
  _id: string,
  title: string,
   description: string,
   price: number,
   image: string,
   category: string,
}


