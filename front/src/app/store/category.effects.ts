import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Router} from "@angular/router";
import {CategoryService} from "../services/category.service";
import {fetchCategoryFailure, fetchCategoryRequest, fetchCategorySuccess} from "./category.actions";
import {catchError, map, mergeMap, of} from "rxjs";



@Injectable()
export class CategoryEffects {

  constructor(
    private actions: Actions,
    private categoryService: CategoryService,
    private router: Router
  ) {}

  fetchCategories = createEffect(() => this.actions.pipe(
    ofType(fetchCategoryRequest),
    mergeMap(() => this.categoryService.getCategories().pipe(
      map(categories => fetchCategorySuccess({categories})),
      catchError(() => of(fetchCategoryFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

}
