import {createAction, props} from "@ngrx/store";
import {Category} from "../models/category.model";


export const fetchCategoryRequest = createAction('[Categories] fetch Request');
export const fetchCategorySuccess = createAction('[Categories] fetch Success',
  props<{categories: Category[]}>()
)
export const fetchCategoryFailure = createAction('[Categories] fetch Failure',
  props<{error: string}>()
);
