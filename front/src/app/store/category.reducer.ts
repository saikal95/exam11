import {CategoryState, ProductState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createProductFailure,
  createProductRequest,
  createProductSuccess,
  deleteProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess
} from "./products.actions";
import {fetchCategoryFailure, fetchCategoryRequest, fetchCategorySuccess} from "./category.actions";

const initialState: CategoryState = {
  categories: [],
  fetchLoading: false,
  fetchError: null,
};


export const categoryReducer = createReducer(
  initialState,
  on(fetchCategoryRequest, state => ({...state, fetchLoading: true})),
  on(fetchCategorySuccess, (state, {categories}) => ({
    ...state,
    fetchLoading: false,
    categories
  })),
  on(fetchCategoryFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),


)
