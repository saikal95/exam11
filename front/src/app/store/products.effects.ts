import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {ProductsService} from "../services/products.service";
import {Router} from "@angular/router";
import {
  createProductFailure,
  createProductRequest,
  createProductSuccess, deleteProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  fetchProductFailure,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess
} from "./products.actions";
import {catchError, map, mergeMap, of, tap} from "rxjs";


@Injectable()
export class ProductsEffects {

  constructor(
    private actions: Actions,
    private productService: ProductsService,
    private router: Router
  ) {}

  fetchProducts = createEffect(() => this.actions.pipe(
    ofType(fetchProductsRequest),
    mergeMap(() => this.productService.getProducts().pipe(
      map(products => fetchProductsSuccess({products})),
      catchError(() => of(fetchProductsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));


  createProduct = createEffect(() => this.actions.pipe(
    ofType(createProductRequest),
    mergeMap(({productData, token}) => this.productService.createProduct(productData, token).pipe(
      map(() => createProductSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createProductFailure({error: 'Wrong data'})))
    ))
  ));


  createOneProduct = createEffect(() => this.actions.pipe(
    ofType(fetchProductRequest),
    mergeMap(({id}) => this.productService.getProduct(id).pipe(
      map((product) => fetchProductSuccess({product})),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(fetchProductFailure({error: 'Wrong data'})))
    ))
  ));


  deleteProduct = createEffect(() => this.actions.pipe(
    ofType(deleteProductRequest),
    mergeMap(({id, token}) => this.productService.deleteProduct(id, token).pipe(
      map(() => deleteProductSuccess()),
      catchError(() => of(deleteProductFailure({
        error: 'Something went wrong'
      })))
    ))
  ));




}
