
import {ProductState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createProductFailure,
  createProductRequest,
  createProductSuccess,
  deleteProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess
} from "./products.actions";


const initialState: ProductState = {
  products: [],
  product: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  deleteLoading: false,
  deleteError: null,
};

export const productReducer = createReducer(
  initialState,
  on(fetchProductsRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductsSuccess, (state, {products}) => ({
    ...state,
    fetchLoading: false,
    products
  })),
  on(fetchProductsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createProductRequest, state => ({...state, createLoading: true})),
  on(createProductSuccess, state => ({...state, createLoading: false})),
  on(createProductFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(fetchProductRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductSuccess, (state, {product}) => ({
    ...state,
    fetchLoading: false,
    product
  })),
  on(fetchProductsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(deleteProductRequest, state => ({...state, fetchLoading: true})),
  on(deleteProductSuccess, (state, ) => ({
    ...state,
    deleteLoading: false,
  })),
  on(deleteProductFailure, (state, {error}) => ({
    ...state,
    deleteLoading: false,
  })),

)
