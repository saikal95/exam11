import {createAction, props} from "@ngrx/store";
import {Product, ProductData} from "../models/product.model";


export const fetchProductsRequest = createAction('[Products] fetch Request');
export const fetchProductsSuccess = createAction('[Products] fetch Success',
  props<{products: Product[]}>()
)
export const fetchProductsFailure = createAction('[Products] fetch Failure',
  props<{error: string}>()
);


export const fetchProductRequest = createAction('[Product] fetch OneRequest',
  props<{id: string }>()
  );
export const fetchProductSuccess = createAction('[Product] fetch  OneSuccess',
  props<{product: Product}>()
)
export const fetchProductFailure = createAction('[Product] fetch OneFailure',
  props<{error: string}>()
);

export const deleteProductRequest = createAction('[Product] delete OneRequest',
  props<{id: string, token: string}>()
);
export const deleteProductSuccess = createAction('[Product] delete  OneSuccess',
)
export const deleteProductFailure = createAction('[Product] delete OneFailure',
  props<{error: string}>()
);

export const createProductRequest = createAction(
  '[Product] Create Request',
  props<{productData: ProductData, token : string}>()
);
export const createProductSuccess = createAction(
  '[Product] Create Success'
);
export const createProductFailure = createAction(
  '[Posts] Create Failure',
  props<{error: string}>()
);

