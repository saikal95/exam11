const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ProductsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
    validate: {
      validator: async function (value)  {
        if (value < 0) return false
        return true
      },
    }
  },
  image: {
    type: String,
    required: true,
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
});

const Product = mongoose.model('Product', ProductsSchema);

module.exports = Product;