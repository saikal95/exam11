const express = require('express');
const Product = require("../models/Products");
const multer = require('multer');
const config = require('../config');
const path = require("path");
const {nanoid} = require('nanoid');
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};

      if(req.query.category){
        query.category = req.query.category
      }
    const products = await Product.find(query).populate("user", "displayName, phoneNumber");
    return res.send(products);
  } catch (e) {
    next(e);
  }
});


router.post('/',auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body) {
      return res.status(400).send({message: 'You should insert all fields'});
    }

    const productData = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.description,
      image: req.body.image,
      category: req.category._id,
      user: req.user._id,
    };

    if (req.file) {
      req.image = req.file.filename;
    }
    const product = new Product(productData);


    await product.save();

    return res.send({message: 'Created new post', id: product._id});
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const product = await  Product.findById(req.params.id).populate("user", "displayName");
    console.log(product);
    if(!product){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(product);

  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const product = await  Product.findByIdAndDelete(req.params.id);
    if(!product){
      return res.status(404).send({message: 'Not product hence the product can not be deleted'});
    }
    return res.send({message: 'Product is deleted!!!'});

  } catch (e) {
    next(e);
  }
});



module.exports = router;

