const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Product = require("./models/Products");
const Category = require("./models/Categories");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [SaikalMuratbekkyzy, JohnDoe] = await User.create({
    userName: 'Saikal Muratbek kyzy',
    password: '1234',
    displayName: 'Saiko',
    phoneNumber: '551332234',
    token: '1234abc'
  }, {
    userName: 'John Doe',
    password: '1234',
    displayName: 'Johny',
    phoneNumber: '551332235',
    token: '1235abc'
  });

  const[Computer, Cars] = await Category.create({
    typeOf: 'Computer'
  },{
    typeOf: 'Car'
  })

  await Product.create({
    title: 'Asus Zenbook',
    description: 'Very good and nice computer',
    price: 400,
    image: "cpu.jpeg",
    category: Computer,
    user: SaikalMuratbekkyzy,
  },{
    title: 'Ferrari',
    description: 'Very good and cheap car',
    price: 500,
    image: "car.jpg",
    category: Cars,
    user: JohnDoe,

  })


  await mongoose.connection.close();
};

run().catch(e => console.error(e));